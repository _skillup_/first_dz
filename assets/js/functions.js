'use strict';

function getLastElements(array, n) {

	if (!Array.isArray(array)) {
		return 'Must be array!';
	}

	var	n			= (typeof n === 'number' && n) ? n : false,
		totalItems	= array.length;

	if (totalItems) {
		if (totalItems < n) {
			return `Array has only ${totalItems} items!`;
		} else if (totalItems === n) {
			return array;
		} else {
			return array.slice(totalItems - n, totalItems);
		}
	}

	return array.pop();

}

function stringToArray(str, separator) {

	var str			= (typeof str === 'string' && str) ? str : false,
		separator	= separator || ' ';

	if (str) {
		return str.split(separator);
	}

	return 'Empty string!';

}