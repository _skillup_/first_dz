'use strict';

class Gallery {

	constructor(config, images) {

		this.images = images;

		this.mainImageSelector		= (document.querySelector(config.mainImageSelector))		|| false;
		this.thumbSelector			= (document.querySelector(config.thumbSelector))			|| false;
		this.overlaySelector		= (document.querySelector(config.overlaySelector))			|| false;
		this.buttonOverlaySelector	= (document.querySelector(config.buttonOverlaySelector))	|| false;

	}

	initialize() {

		if (!this.images) {
			console.log('No images!');
			return false;
		}

		if (!this.mainImageSelector) {
			console.log('Main image selector must be setted!');
			return false;
		}

		this.MainImage = this.images[0];
		this.setMainImage();

		for (let image of this.images) {
			this.appendImages(image);
		}

		this.buttonOverlaySelector.onclick = (e) => {
			this.initOverlay();
		}

	}

	initOverlay() {

		let buttonClasses = this.buttonOverlaySelector.classList;

		if (buttonClasses.contains('dark')) {
			buttonClasses.remove('dark');
			buttonClasses.add('light');
			this.buttonOverlaySelector.textContent = 'Висвітлити';
			this.overlaySelector.style.backgroundColor = 'rgba(0,0,0, 0.5)';
		} else if (buttonClasses.contains('light')) {
			buttonClasses.remove('light');
			buttonClasses.add('dark');
			this.buttonOverlaySelector.textContent = 'Затемнити';
			this.overlaySelector.style.backgroundColor = 'rgba(0,0,0, 0)';
		}

	}

	setMainImage() {
		this.mainImageSelector.setAttribute('src', this.MainImage);
	}

	appendImages(image) {

		let newImage = document.createElement('img');
		newImage.setAttribute('src', image);

		newImage.onclick = (e) => {
			this.MainImage = e.target.getAttribute('src');
			this.setMainImage();
		}

		this.thumbSelector.appendChild(newImage);

	}

}

const gallery = new Gallery(
	{
		mainImageSelector:		'.displayed-img',
		thumbSelector:			'.thumb-bar',
		overlaySelector:		'.overlay',
		buttonOverlaySelector:	'button'
	},
	[
		'images/gallery/1w2gfy.jpg',
		'images/gallery/CimY1xTWsAELmP7.jpg',
		'images/gallery/open-your-front-facing-camera.jpg',
		'images/gallery/persian.jpg',
		'images/gallery/wild-cats-apple-mac-comic-pet-very-happy-cat_467784.jpg'
	]
).initialize();